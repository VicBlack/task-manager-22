package ru.t1.kupriyanov.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void showHelp();

    void showCommands();

    void showArguments();

    void showAbout();

    void showVersion();

    void exit();

    void showWelcome();

}
