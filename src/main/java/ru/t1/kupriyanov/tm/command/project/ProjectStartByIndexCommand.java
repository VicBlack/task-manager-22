package ru.t1.kupriyanov.tm.command.project;

import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Start project by index.";
    }

}
