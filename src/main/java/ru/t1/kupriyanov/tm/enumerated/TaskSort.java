package ru.t1.kupriyanov.tm.enumerated;

import ru.t1.kupriyanov.tm.comparator.CreatedComparator;
import ru.t1.kupriyanov.tm.comparator.NameComparator;
import ru.t1.kupriyanov.tm.comparator.StatusComparator;
import ru.t1.kupriyanov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    private final String displayName;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort taskSort : values()) {
            if (taskSort.name().equals(value)) return taskSort;
        }
        return null;
    }

    TaskSort(String displayName, Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    public Comparator<Task> getComparator() {
        return comparator;
    }

}
