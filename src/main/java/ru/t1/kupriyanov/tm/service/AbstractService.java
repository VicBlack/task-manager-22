package ru.t1.kupriyanov.tm.service;

import ru.t1.kupriyanov.tm.api.repository.IRepository;
import ru.t1.kupriyanov.tm.api.service.IService;
import ru.t1.kupriyanov.tm.exception.entity.ModelNotFoundException;
import ru.t1.kupriyanov.tm.exception.field.IdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.IndexIncorrectException;
import ru.t1.kupriyanov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M removeOne(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(model);
    }

    @Override
    public M removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = findOneById(id);
        return removeOne(model);
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final M model = findOneByIndex(index);
        return removeOne(model);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

}
