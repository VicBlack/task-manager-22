package ru.t1.kupriyanov.tm.repository;

import ru.t1.kupriyanov.tm.api.repository.ITaskRepository;
import ru.t1.kupriyanov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return records
                .stream()
                .filter(m -> m.getProjectId() != null)
                .filter(m -> projectId.equals(m.getProjectId()))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
